import curses
import itertools

from ._util import get_content_size
from .Style import Alignment, SizeType
from .BoxStyle import BoxStyle


class Box(BoxStyle):
    def __init__(self):
        self.content = []
        self.height = None
        self.width = None
        self.x = None
        self.y = None

    def add_content(self, obj, idx=None):
        self.content.insert(idx if idx else len(self.content), obj)

    def remove_content(self, idx):
        del self.content[idx]

    def rescale(self):
        """ TODO:... """
        if self.alignment == Alignment.LEFT:
            width_content = iter(get_content_size(self.width, [i.size for i in self.content], [i.size_type for i in self.content]))
            heigh_content = itertools.repeat(self.height, len(self.content))
        elif self.alignment == Alignment.TOP:
            width_content = itertools.repeat(self.width, len(self.content))
            heigh_content = iter(get_content_size(self.height, [i.size for i in self.content], [i.size_type for i in self.content]))
        else:
            raise NotImplementedError("This alignment is not implemented yet.")

        for item, width, heigh in zip(self.content, width_content, heigh_content):
            item.width = width
            item.height = heigh

        # invoke rescale on every object in content
        for obj in self.content:
            obj.rescale()

    def reposition(self):
        """ TODO:... """
        x = self.x
        y = self.y

        # reposition all content objects
        for obj in self.content:
            obj.x = x
            obj.y = y

            if self.alignment == Alignment.LEFT:
                x += obj.width
            elif self.alignment == Alignment.TOP:
                y += obj.height
            else:
                raise NotImplementedError("this alignment is not implemented yet")

        # invoke rescale on every object in content
        for obj in self.content:
            obj.reposition()

    def fill(self, screen=None, char="*"):
        """ TODO:... """
        for line in range(self.y, self.y + self.height):
            screen.insstr(line, self.x, char * self.width)

