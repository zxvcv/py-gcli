import os
import sys
import json
import clint
import random
import curses
from time import sleep
from pathlib import Path

from .Style import Alignment, SizeType
from .Box import Box
from .Table import Table
from .Window import Window


class SuperBox():
    def __init__(self):
        pass

    def rescale(self):
        pass

def test4():
    with Path("C:/Users/ppisk/WS/py-gcli/src/gcli/test_data.json").open() as f:
        test_data = json.load(f)

    # window
    wd = Window()
    wd.size_type = SizeType.PERCENTAGE
    wd.size = 100
    wd.alignment = Alignment.LEFT

    # left bar
    left_bar = Box()
    left_bar.size_type = SizeType.PERCENTAGE
    left_bar.size = 25
    left_bar.alignment = Alignment.LEFT

    # middle
    middle_box = Box()
    middle_box.size_type = SizeType.PERCENTAGE
    middle_box.size = 50
    middle_box.alignment = Alignment.TOP

    track_list = Table(
        labels=test_data["labels"],
        data=test_data["data"]
    )
    for i, w in enumerate(test_data["labels_width"]):
        track_list.labels[i].width = w
    for i, w in enumerate(test_data["labels_width_type"]):
        if w == "PERCENTAGE":
            track_list.labels[i].width_type = SizeType.PERCENTAGE
        elif w == "ACCURATE":
            track_list.labels[i].width_type = SizeType.ACCURATE
        else:
            track_list.labels[i].width_type = None
    for i, w in enumerate(test_data["labels_left_bound"]):
        track_list.labels[i].l_bound = w
    for i, w in enumerate(test_data["labels_right_bound"]):
        track_list.labels[i].r_bound = w
    track_list.alignment = Alignment.LEFT
    track_list.size_type = SizeType.PERCENTAGE
    track_list.size = 50

    player_monitor = Box()
    player_monitor.size_type = SizeType.PERCENTAGE
    player_monitor.size = 50
    player_monitor.alignment = Alignment.LEFT

    # right bar
    right_bar = Box()
    right_bar.size_type = SizeType.PERCENTAGE
    right_bar.size = 25
    right_bar.alignment = Alignment.LEFT


    # placing
    wd.add_content(left_bar)
    middle_box.add_content(track_list)
    middle_box.add_content(player_monitor)
    wd.add_content(middle_box)
    wd.add_content(right_bar)




    wd.rescale()
    wd.reposition()

    # wd.update()
    # wd.draw()


    left_bar.fill(wd.screen, "!")
    track_list.update()
    track_list.draw(wd.screen)
    player_monitor.fill(wd.screen, "^")
    right_bar.fill(wd.screen, "$")

    wd.refresh()
    curses.napms(10000)

def test3():
    wd = Window()
    wd.size = 100
    wd.alignment_axis = True

    inside_box1 = Box()
    inside_box1.size = 30
    inside_box1.alignment_axis = True
    wd.add_content(inside_box1)

    inside_box2 = Box()
    inside_box2.size = 70
    inside_box2.alignment_axis = True
    wd.add_content(inside_box2)

    wd.rescale()
    wd.reposition()

    inside_box2.fill(wd.screen, "#")

    wd.refresh()
    curses.napms(1000)

    counter = 0
    if counter < 10:
        ch = wd.screen.getch()
        # if wd.is_size_changed():
        if ch == curses.KEY_RESIZE:
            print(f"HEIGHT:{wd.height} WIDTH:{wd.width}")
            wd.rescale()
            wd.reposition()

            # inside_box1.fill(wd.screen, "!")
            inside_box2.fill(wd.screen, "#")

            wd.refresh()
        curses.napms(1000)


def test2():
    wd = Window()
    wd.size = 100
    wd.alignment_axis = True

    inside_box1 = Box()
    inside_box1.size = 30
    inside_box1.alignment_axis = True
    wd.add_content(inside_box1)

    inside_box2 = Box()
    inside_box2.size = 70
    inside_box2.alignment_axis = True
    wd.add_content(inside_box2)

    wd.rescale()
    wd.reposition()
    # wd.draw()

    # inside_box1.fill(wd.screen, "!")
    inside_box2.fill(wd.screen, "#")

    wd.refresh()
    curses.napms(10000)

def test1():
    screen = curses.initscr()

    # Initialize color in a separate step
    curses.start_color()

    # Change style: bold, highlighted, and underlined text
    screen.addstr("Regular text\n")
    screen.addstr("Bold\n", curses.A_BOLD)
    screen.addstr("Highlighted\n", curses.A_STANDOUT)
    screen.addstr("Underline\n", curses.A_UNDERLINE)
    screen.addstr("Regular text again\n")

    # Create a custom color set that you might re-use frequently
    # Assign it a number (1-255), a foreground, and background color.
    curses.init_pair(1, curses.COLOR_RED, curses.COLOR_WHITE)
    screen.addstr("RED ALERT!\n", curses.color_pair(1))

    # Combine multiple attributes with bitwise OR
    screen.addstr("SUPER RED ALERT!\n", curses.color_pair(1) | curses.A_BOLD | curses.A_UNDERLINE | curses.A_BLINK)

    screen.border()
    screen.hline(10, 0, "-", 20)
    screen.refresh()
    curses.napms(3000)

def main(argv=sys.argv[1:]):
    # for i in clint.textui.progress.bar(range(100)):
    #     sleep(1)
    # bar.show(1)
    # print(os.get_terminal_size())
    # print(curses.KEY_RESIZE)

    # screen = curses.initscr()

    # while True:
    #     sleep(1)
    #     screen.addstr("*")
    #     c = screen.getch()
    #     if c == curses.KEY_RESIZE:
    #         screen.addstr("*")

    # test1()
    # test2()
    # test3()
    test4()


# if __name__.rpartition(".")[-1] == "__main__":
#     sys.exit(main(sys.argv[1:]))
