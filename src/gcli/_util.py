from math import floor

from .Style import SizeType


def get_content_size(parent_size:int, content_size:list, content_size_type:list) -> list:
    accurate_size = []
    chars_to_spread = 0

    # get size of each value
    for size, typ in zip(content_size, content_size_type):
        if typ == SizeType.PERCENTAGE:
            acc_size = parent_size * size / 100
            if acc_size > floor(acc_size):
                chars_to_spread += 1
            acc_size = floor(acc_size)
        elif typ == SizeType.ACCURATE:
            acc_size = size
        else:
            raise ValueError("Unknown SizeType.")

        accurate_size.append(acc_size)

    # spread reast of the peaces
    # TODO: add priority for elements spreading
    for i in range(len(accurate_size)):
        if chars_to_spread <= 0:
            break

        if content_size_type == SizeType.PERCENTAGE:
            chars_to_spread -= 1
            accurate_size[i] += 1

    # check if sum content size is not bigger than parent_size
    if sum(accurate_size) > parent_size:
        raise ValueError("Content summary size is bigger than available parent size.")

    return accurate_size
