from enum import Enum


class TableLabel():
    def __init__(self, id, name):
        self.id = id
        self.name = name
        self.width_type = None
        self.width = None #% total size
        self.l_bound = ""
        self.r_bound = ""

        # size of label in actual chars
        self.size = None # total size of label
        self.size_data = None # size of data in chars of label
