import curses

from .Box import Box


class DrawableBox(Box):
    def __init__(self):
        super().__init__()
        self.data_generator = None

    def draw(self, screen=None):
        for i, val in enumerate(self.update()):
            if self.width < 3:
                val = " " * self.width
            elif len(val) <= self.width:
                val = "{}{fill}".format(val, fill=" " * (self.width - len(val)))
            else: # len(val) > self.width:
                val = "{}{fill}".format(val[:self.width-3], fill="/..")

            screen.insstr(self.y + i, self.x, val)
