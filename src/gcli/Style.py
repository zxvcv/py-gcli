from abc import ABC
from enum import Enum


class SizeType(Enum):
    PERCENTAGE = 1
    ACCURATE = 2

class Alignment(Enum):
    LEFT = 1
    CENTER_HORIZONTAL = 2
    RIGHT = 3
    TOP = 4
    CENTER_VERTICAL = 5
    BOTTOM = 6

class Style(ABC):
    def __init__(self):
        self.alignment = None
        self.size_type = None

    @property
    def size(self):
        return self.__size

    @size.setter
    def size(self, val:int):
        """ Size of the object in size_type units.

            Attributes:
                val: size value

            Raises:
                ValueError if value is not correct
        """
        if self.size_type and self.size_type == SizeType.PERCENTAGE:
            if not (0 <= val <= 100):
                raise ValueError("Percentage dalue needs to be in range [0:100].")
        self.__size = val

    @property
    def display(self):
        return self.__display

    @display.setter
    def display(self, val:bool):
        """ Determine that object is visible or not.

            Attributes:
                val: True=object visible, False=object hidden
        """
        self.__display = val
