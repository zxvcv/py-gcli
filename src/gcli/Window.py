import curses

from .Box import Box
from .Style import Style


class Window(Box, Style):
    def __init__(self):
        super().__init__()
        self.screen = curses.initscr()

    def is_size_changed(self) -> bool:
        return curses.is_term_resized(self.height, self.width)
        height, width = curses.initscr().getmaxyx()
        print(f"!HEIGHT:{height} WIDTH:{width}")
        if (height != self.height) or (width != self.width):
            return True
        return False

    def rescale(self):
        # setup this object size values as current terminal size
        self.height, self.width = self.screen.getmaxyx()
        print(f"HEIGHT:{self.height} WIDTH:{self.width}")
        super().rescale()

    def reposition(self):
        self.x = 0
        self.y = 0
        super().reposition()

    def update(self):
        pass

    def draw(self, screen=None):
        for obj in self.content:
            obj.draw(screen if screen else self.screen)

    def refresh(self):
        self.screen.refresh()
