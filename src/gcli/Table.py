import curses

from typing import List

from ._util import get_content_size
from .Style import SizeType
from .TableLabel import TableLabel
from .DrawableBox import DrawableBox


class Table(DrawableBox):
    _MIN_COL_SIZE = 5

    def __init__(self, data:list, labels:List[str]):
        super().__init__()

        self.data = data
        self.labels = []
        for id, label in enumerate(labels):
            self.labels.append(TableLabel(id, label))

    def set_labels_size(self, width, labels:list):
        """ Sets passed labels in width and returns labels as generator.
            Size of left and right bounds will not be added to the field size.
        """
        content_size = get_content_size(width, [i.width for i in labels], [i.width_type for i in labels])

        for label, size in zip(labels, content_size):
            label.size = size
            label.size_data = size - len(label.l_bound) - len(label.r_bound)

            if label.size_data <= 0:
                raise ValueError("Data place size is too small.")

            yield label

    def update(self):
        if len(self.labels) == 0:
            # TODO: print "NO DATA in the middle of the Box space"
            return

        # TODO: check if data are same length in all labels

        # labels to be printed
        labels_tbp = self.labels
        rows_tbp = self.data

        # check size values
        if sum([label.width for label in labels_tbp]) > 100:
            raise ValueError("Summary of printed collumns sizes must be in range [0:100](%).")

        template = ""
        for label in self.set_labels_size(self.width, self.labels):
            # create templates
            template += "{}{:" + str(label.size_data) + "}{}"

        # return lables
        row_data = []
        for label in labels_tbp:
            val = str(label.name)
            row_data.append(str(label.l_bound))
            row_data.append(str(val[:label.size_data-2] + '..') if len(val) > label.size_data else val)
            row_data.append(str(label.r_bound))

        yield template.format(*row_data)
        yield '-' * self.width

        # return data
        for row in rows_tbp:
            row_data = []
            for label in labels_tbp:
                # value of specific label
                val = str(row[label.id])

                # cutting value of data that are longer than size of row
                row_data.append(str(label.l_bound))
                row_data.append(str(val[:label.size_data-2] + '..') if len(val) > label.size_data else val)
                row_data.append(str(label.r_bound))

            yield template.format(*row_data)

        # fill rest space with empty chars (spaces)
        for i in range(2 + len(rows_tbp), self.height):
            yield " " * self.width
