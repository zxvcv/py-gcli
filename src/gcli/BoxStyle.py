from abc import ABC

from .Style import Style


class BoxStyle(Style, ABC):
    def __init__(self):
        super().__init__()
