Changelog
=========

0.0.7 (2021-12-25)
------------------
- get_content_size() moved to _util.py.
- Table set_labels_size() implementation with get_content_size().

0.0.6 (2021-12-25)
------------------
- Improvements in Box rescale.
- WidthType enum renamed to SizeType.
- SizeType moved to Style.py file.

0.0.5 (2021-12-24)
------------------
- Improvements in Box rescale.
- Table Labels size can be set as PERCENTAGE or ACCURATE value.

0.0.4 (2021-12-23)
------------------
- Add custom bounds for Table.

0.0.3 (2021-12-21)
------------------
- Rework of Table update().

0.0.2 (2021-12-20)
------------------
- Fix for reposition(), rescale().

0.0.1 (2021-12-17)
------------------
- draw() moved to DrawableBox class.
- Functionality of draw() moved to new update() method in Table.

0.0.0 (2021-12-14)
------------------
- Initial commit.
